const { getQueueTasks, startTaskComputing, finishedTaskComputing } = require('./../store');
const config = require('./../../config');
const worker = createPanel();

function createPanel() {
    let isOnline = false;
    return {
        setOnline: () => isOnline = true,
        setOffline: () => isOnline = false,
        isOnline: () => isOnline
    }
}

function generateNumber({min, max}) {
    return ~~(Math.random() * (max - min + 1) + min);
}

function startComputing() {
    if (!worker.isOnline()) computing();
}

async function computing() {
    worker.setOnline();

    for (let task of getQueueTasks())
        await heavyComputing({startTaskComputing, task, finishedTaskComputing});

    if (getQueueTasks().length > 0) computing();
    else worker.setOffline();
}

async function heavyComputing({startTaskComputing, task, finishedTaskComputing}) {
    startTaskComputing();
    return new Promise(resolve => {
        let computingTime = generateNumber(config.computingTime);

        setTimeout(() => {
            finishedTaskComputing(computingTime);
            resolve();
        }, computingTime * 1000);
    })
}


module.exports = {
    startComputing
};