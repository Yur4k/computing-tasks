const config = require('./../../config');

const status = {
    IN_PROGRESS: 'in_progress',
    IN_QUEUE: 'in_queue',
    DONE: 'done',
};
let tasksQueue = [];
let tasks = [];
let avgComputingTime = 0;
let currentTask = null;

function getQueueCount() {
    return tasksQueue.length;
}

function getQueueTasks() {
    return tasksQueue;
}

async function createTask() {
    return new Promise((resolve, reject) => {
        let tasksCount = tasksQueue.length;

        if(tasksCount >= config.maxServerLoad) return reject({id: null, message: `Server is busy! max load ${config.maxServerLoad} tasks`});

        let etd = avgComputingTime * (tasksCount > 0
            ? tasksCount + 2
            : currentTask ? 2 : 1);
        let task = { id: Date.now(), etd };
        tasksQueue.push(task);

        resolve(task);
    });
}

function startTaskComputing() {
    currentTask = tasksQueue[0];
    tasksQueue = tasksQueue.length === 1
        ? []
        : tasksQueue.slice(1, tasksQueue.length);
}

function finishedTaskComputing(computingTime) {
    avgComputingTime = avgComputingTime === 0
        ? computingTime
        : (avgComputingTime + computingTime) / 2;
    tasks = [{ id: currentTask.id, computingTime}, ...tasks];
    currentTask = null;
}

function checkTask({id}) {
    if (currentTask && currentTask.id === id) return {id: id, status: status.IN_PROGRESS, etd: avgComputingTime};

    let taskFinishedIndex = tasks.findIndex(task => task.id === id);
    if (taskFinishedIndex > -1)
        return {id: id, status: status.DONE, computing_time: tasks[taskFinishedIndex].computingTime};

    let taskQueueIndex = tasksQueue.findIndex(task => task.id === id);
    return taskQueueIndex > -1
        ? {id: id, status: status.IN_QUEUE, position: taskQueueIndex + 1, etd: avgComputingTime * (taskQueueIndex + 2)} // +1 for self-computing
        : {id: null, message: 'Task not exists'}
}

async function getTasks() {
    let tasksQueueWithTime = tasksQueue.map((task, index) => ({
        ...task,
        etd: avgComputingTime * (index + 2) // +1 for index(0), +1 for self-computing time
    }));

    return {
        task_in_progress: currentTask && { ...currentTask, etd: avgComputingTime } || 'no one',
        tasks_queue: tasksQueueWithTime,
        tasks_finished: tasks
    };
}


module.exports = {
    createTask,
    checkTask,
    getTasks,
    getQueueTasks,
    getQueueCount,
    startTaskComputing,
    finishedTaskComputing,
};