const { createTask } = require('./../../libs/store');
const { startComputing } = require('./../../libs/heavy_computing');

module.exports = function (server) {
    server.get('/create', function (req, res) {
        createTask()
            .then(task => {
                startComputing();
                res.json(task);
            })
            .catch(error => {
                res.json(error)
            });
    });
};