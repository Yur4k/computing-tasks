const { checkTask } = require('./../../libs/store');

module.exports = function (server) {
    server.get('/check/:id', function (req, res) {
        let id = Number.parseInt(req.params.id);

        if (Number.isInteger(id)) {
            res.json(checkTask({ id }));
        } else {
            res.json({id: null, message: 'Invalid task is. ID must be a number'})
        }
    });
};