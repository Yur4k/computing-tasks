module.exports = function (server) {
    require('./create')(server);
    require('./check')(server);
    require('./list')(server);
};