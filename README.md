## About this project
 * Server emulation work with computing task
 * Tasks queue
 * Getting task info 
 
### Project info
####
        ./config              - config file with [computing_time] and [max_server_load] params
        ./controllers         - endpoints controllers
            ./index.js        - barrel for all controllers
        ./libs                - libs of project
            ./heavy_computing - emulating heavy computing task
            ./store           - store and tools for work with tasks
        ./index.js            - start server
    
        GET /create    - created new task if server not busy
        GET /check/123 - get info about task by id
        GET /list      - get all tasks