const computingTime = { min: 30, max: 90 };
const maxServerLoad = 10; //max tasks in queue

module.exports = {
    computingTime,
    maxServerLoad
};