const { getTasks } = require('./../../libs/store');

module.exports = function (server) {
    server.get('/list', function (req, res) {
        getTasks()
            .then(tasks => res.json(tasks))
            .catch(error => res.json({ error }))
    });
};